### What is this repository for? ###

This is a experimental project written in C# (MVC 4). 

It's purpose is to illustrate how AngularJS and SignalR can be used together to build single page applications that support real-time user collaboration.


### How do I get set up? ###

**Requirements**

* Visual Studio 2013 or higher.
* .NET Framework 4.5 or higher.
* Any modern web browser (IE9+, latest version of Chrome, Safari, Firefox, Opera).

**How to Build**

* Checkout the repository  `git clone https://bitbucket.org/nfowlie/tictactoe.git`.
* Open the tictactoe.sln in Visual Studio and build! If you get dependency errors, make sure you've enabled nuget package restore at the project level.


### Contribution guidelines ###

This project is a SignalR + AngularJS self-learning exercise as much as it is a technology demonstration, so I'm not taking contributions at the moment. This is likely to change once the project is complete and I start looking for areas where the code can be improved/optimised.


### Who do I talk to? ###

If you have any questions or would like to offer improvement suggestions, please contact Nathon Fowlie (<nathon.fowlie@gmail.com>).